package jit.models;

import jit.utils.NumberUtil;
import jit.utils.POIUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;

public class WCell {
    private String rLabel = "";
    private String cLabel = "";
    private String address = "";
    private String formula = "";
    private String showValue = "";
    private double actualValue = 0.0;

    private boolean isFormula = false;
    private boolean isIFFormula = false;
    private boolean isReferFormula = false;
    private boolean hasAccuracyProblem = false;
    private boolean hasDataError=false;


    public WCell(Cell cell) {
        if(cell.getCellType() != CellType.FORMULA &&cell.getCellType()!=CellType.NUMERIC)
            return ;

        String rv="";
        try {
            if(DateUtil.isCellDateFormatted(cell))
                return;

        this.address = cell.getAddress().toString();
        this.showValue= POIUtil.getShowValue(cell);

        if (cell.getCellType() == CellType.FORMULA) {
            this.formula = cell.getCellFormula();
            this.isFormula = true;
            if (this.formula.indexOf("!") != -1)
                this.isReferFormula = true;
            if (this.formula.indexOf("IF") != -1||this.formula.indexOf("if") != -1)
                this.isIFFormula = true;
            this.actualValue=cell.getNumericCellValue();
        }
        if(cell.getCellType()==CellType.NUMERIC){
            this.actualValue=cell.getNumericCellValue();
        }
        if(Double.isNaN(actualValue)||Double.isInfinite(actualValue))
            return;

        if(POIUtil.getAccuracy(actualValue+"")!=POIUtil.getAccuracy(showValue))
            this.hasAccuracyProblem=true;


        if(NumberUtil.parseDouble(showValue)-actualValue!=0)
            this.hasDataError=true;
        }catch (Exception ex){
//            System.out.println(this.address);
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getrLabel() {
        return rLabel;
    }

    public void setrLabel(String rLabel) {
        this.rLabel = rLabel;
    }

    public String getcLabel() {
        return cLabel;
    }

    public void setcLabel(String cLabel) {
        this.cLabel = cLabel;
    }

    public String getShowValue() {
        return showValue;
    }

    public void setShowValue(String showValue) {
        this.showValue = showValue;
    }

    public double getActualValue() {
        return actualValue;
    }

    public void setActualValue(double actualValue) {
        this.actualValue = actualValue;
    }

    public boolean isFormula() {
        return isFormula;
    }

    public void setFormula(boolean formula) {
        isFormula = formula;
    }

    public boolean isIFFormula() {
        return isIFFormula;
    }

    public void setIFFormula(boolean IFFormula) {
        isIFFormula = IFFormula;
    }

    public boolean isReferFormula() {
        return isReferFormula;
    }

    public void setReferFormula(boolean referFormula) {
        isReferFormula = referFormula;
    }

    public boolean isHasAccuracyProblem() {
        return hasAccuracyProblem;
    }

    public void setHasAccuracyProblem(boolean hasAccuracyProblem) {
        this.hasAccuracyProblem = hasAccuracyProblem;
    }

    public boolean isHasDataError() {
        return hasDataError;
    }

    public void setHasDataError(boolean hasDataError) {
        this.hasDataError = hasDataError;
    }
}
