package jit.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WSheet {
    private String name;

    private List<WCell> cells=new ArrayList<>();

    public WSheet(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WCell> getCells() {
        return cells;
    }

    public void setCells(List<WCell> cells) {
        this.cells = cells;
    }

    public void addCell(WCell cell) {
        if(!cells.contains(cell))
            cells.add(cells.size(),cell);
    }

    public List<WCell> getReferenceCell(){
        List<WCell> smells=new ArrayList<>();
        for(WCell cell :cells){
            if(cell.isReferFormula()){
                smells.add(cell);
            }
        }
        return smells;
    }

    public List<WCell> getIFCell(){
        List<WCell> smells=new ArrayList<>();
        for(WCell cell :cells){
            if(cell.isIFFormula()){
                smells.add(cell);
            }
        }
        return smells;
    }

    public List<WCell> getHasDataErrorCell(){
        List<WCell> smells=new ArrayList<>();
        for(WCell cell :cells){
            if(cell.isHasDataError()){
                smells.add(cell);
            }
        }
        return smells;
    }

    public List<WCell> getHasAccuracyProblemCell(){
        List<WCell> smells=new ArrayList<>();
        for(WCell cell :cells){
            if(cell.isHasAccuracyProblem()){
                smells.add(cell);
            }
        }
        return smells;
    }
}
