package jit.models;

import java.util.ArrayList;

import java.util.List;

public class WBook {
    private String path;
    private String name;
    private List<WSheet> sheets=new ArrayList<>();

    public WBook() {

    }

    public WBook(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WSheet> getSheets() {
        return sheets;
    }

    public void setSheets(List<WSheet> sheets) {
        this.sheets = sheets;
    }

    public void addSheet(WSheet sheet) {
       if(!sheets.contains(sheet))
           sheets.add(sheets.size(),sheet);
    }
}
