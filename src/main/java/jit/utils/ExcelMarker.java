package jit.utils;

import com.google.gson.reflect.TypeToken;
import jit.constants.Constants;
import jit.models.WBook;
import jit.models.WCell;
import jit.models.WSheet;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellReference;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ExcelMarker {
    private static   int index=1;


    public static void main(String[] args) {
        List<String> address = Arrays.asList(new String[]{"C3", "B2", "A1"});
        HashMap<String, List<String>> SmellCells = new HashMap<>();
        SmellCells.put("Sheet1", address);
        SmellCells.put("Sheet2", address);
        String path = "D:/test/test.xlsx";
        String out = "D:/test/out/";
        mark(path, SmellCells);
        System.out.println("Done");
    }
    public static void fillter_out_target_excels(String basePath){
        List<String> records= FileManager.readText(Constants.Enron_baseDir+"Result.txt");
        int index=1;
        for (String excel :records){
            WBook book= (WBook) JsonUtil.toJava(excel, new TypeToken<WBook>() {}.getType());
            String newFile=get_new_file_name(book,basePath,index);
            index++;
            FileManager.copyFile(book.getPath(),newFile);
        }

    }

    public static void mark_fillter_out_target_excels(WBook book,String basePath,int index){
            String newFile=get_new_file_name(book,basePath,index);
            ExcelMarker.mark(book,newFile);
    }

    public static String get_new_file_name(WBook book,String basePath,int index){
        String path=book.getPath().replaceAll("\\\\","/");
        String new_file_name=index+"-"+path.substring(path.lastIndexOf('/')+1);
        return basePath+new_file_name;
    }


    public static void mark(WBook wbook, String marked_file) {
        System.out.println(wbook.getPath());

        Workbook workbook = POIUtil.openSpreadsheet(wbook.getPath());
        CreationHelper createHelper = workbook.getCreationHelper();

        CellStyle header_style = workbook.createCellStyle();
        header_style.setAlignment(HorizontalAlignment.CENTER);
        header_style.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle smell_style = workbook.createCellStyle();
        smell_style.setFillForegroundColor(IndexedColors.RED.getIndex());
        smell_style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        CellStyle refered_style = workbook.createCellStyle();
        refered_style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        refered_style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        CellStyle hlink_style = workbook.createCellStyle();
        Font hlink_font = workbook.createFont();
        hlink_font.setUnderline(Font.U_SINGLE);
        hlink_font.setColor(IndexedColors.BLUE.getIndex());
        hlink_style.setFont(hlink_font);

        if (workbook.getSheet("SmellSheet") != null)
            workbook.removeSheetAt(0);

        Sheet SmellSheet = workbook.createSheet("SmellSheet");
        try {
            workbook.setSheetOrder("SmellSheet", 0);
            workbook.setActiveSheet(0);
        } catch (Exception e) {

        }

        Row hrow = SmellSheet.createRow(0);
        for (int c = 0; c < Constants.mark_excel_headers.length; c++) {
            hrow.createCell(c).setCellValue(Constants.mark_excel_headers[c]);
        }
        hrow.setRowStyle(header_style);


        int index = 1;
        for (WSheet sheet : wbook.getSheets()) {
            String sheetName = sheet.getName();
            List<WCell> tSmellCells = sheet.getCells();
            Sheet TargetSheet = workbook.getSheet(sheetName);
            int count=1;
            for (WCell tSmellCell : tSmellCells) {
                count++;
                Row row = SmellSheet.createRow(index);
                row.createCell(0).setCellValue(index + "");
                row.createCell(1).setCellValue(sheetName);
                Cell hlink_cell = row.createCell(2);
                row.createCell(3).setCellValue(tSmellCell.getShowValue());
                row.createCell(4).setCellValue(tSmellCell.getActualValue());
                row.createCell(5).setCellValue(tSmellCell.getFormula());
                Hyperlink link2 = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                link2.setAddress("'" + sheetName + "'!" + tSmellCell.getAddress());
                hlink_cell.setHyperlink(link2);
                hlink_cell.setCellStyle(hlink_style);
                hlink_cell.setCellValue("'" + sheetName + "'!" + tSmellCell.getAddress());

//                CellReference cr = new CellReference(tSmellCell.getAddress());
//                Row trow = TargetSheet.getRow(cr.getRow());
//                Cell TargetCell = trow.getCell(cr.getCol());
//                TargetCell.setCellStyle(smell_style);

//                mark_referred_cell(workbook, tSmellCell.getFormula(), refered_style);
                index++;
                if(index>60000||count>1000)
                    break;
            }
            if(index>60000)
                break;
        }

        POIUtil.saveSpreadsheet(workbook, marked_file);

    }

    private static void mark_referred_cell(Workbook book, String formula, CellStyle refered_style) {
//        System.out.println(formula);
        if(formula.indexOf("!")==-1)
            return;
        String temps[] = formula.split("!");
        Sheet ReferSheet = book.getSheet(temps[0]);
        CellReference cr = new CellReference(temps[1]);
        Row row = ReferSheet.getRow(cr.getRow());
        if (row == null)
            row = ReferSheet.createRow(cr.getRow());
        Cell TargetCell = row.getCell(cr.getCol());
        if (TargetCell == null)
            TargetCell = row.createCell(cr.getCol());
        TargetCell.setCellStyle(refered_style);

    }

    public static Workbook mark(Workbook workbook, HashMap<String, List<String>> SmellCells) {

        CreationHelper createHelper = workbook.getCreationHelper();

        CellStyle smell_style = workbook.createCellStyle();
        smell_style.setFillForegroundColor(IndexedColors.RED.getIndex());
        smell_style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        CellStyle hlink_style = workbook.createCellStyle();
        Font hlink_font = workbook.createFont();
        hlink_font.setUnderline(Font.U_SINGLE);
        hlink_font.setColor(IndexedColors.BLUE.getIndex());
        hlink_style.setFont(hlink_font);

        if (workbook.getSheet("SmellSheet") != null)
            workbook.removeSheetAt(0);

        Sheet SmellSheet = workbook.createSheet("SmellSheet");
        workbook.setSheetOrder("SmellSheet", 0);
        workbook.setActiveSheet(0);
        int index = 1;
        for (String sheetName : SmellCells.keySet()) {
            List<String> tSmellCells = SmellCells.get(sheetName);
            Sheet TargetSheet = workbook.getSheet(sheetName);
            for (String SmellCell : tSmellCells) {
                Row row = SmellSheet.createRow(index);
                row.createCell(0).setCellValue(index + "");
                row.createCell(1).setCellValue(sheetName);
                Cell hlink_cell = row.createCell(2);

                Hyperlink link2 = createHelper.createHyperlink(HyperlinkType.DOCUMENT);
                link2.setAddress("'" + sheetName + "'!" + SmellCell);
                hlink_cell.setHyperlink(link2);
                hlink_cell.setCellStyle(hlink_style);
                hlink_cell.setCellValue("'" + sheetName + "'!" + SmellCell);

                CellReference cr = new CellReference(SmellCell);
                row = TargetSheet.getRow(cr.getRow());
                Cell TargetCell = row.getCell(cr.getCol());
                TargetCell.setCellStyle(smell_style);
                index++;
            }
        }
        return workbook;

    }

    public static Workbook mark(String filePath, HashMap<String, List<String>> SmellCells) {
        Workbook workbook = POIUtil.openSpreadsheet(filePath);
        return mark(workbook, SmellCells);

    }

}
