package jit.utils;

import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FloderScanner {
    private  String path = "D:\\EUSES\\";
    private  Integer count = 0;

    public static void main(String[] args) {
        FloderScanner scanner =new FloderScanner("");
        scanner.startScan();
        System.out.println();
        System.exit(0);
    }
    public FloderScanner(String path){
        this.path=path;
    }

    public List<String> startScan(){
        return scan(path);
    }


    private  List<String> scan(String path) {
        List<String> results=new ArrayList<>();
        File dir = new File(path);
        File groups[] = dir.listFiles();
        for (File group : groups) {
            if (group.isFile()) {
                String name = group.getName();
//                System.out.println(name);
                if (name.endsWith(".xls") || name.endsWith(".xlsx")) {
                    count++;
                    String filename=group.getAbsolutePath();
                    results.add(filename);
                }
            } else if (group.isDirectory()) {
                if("duplicates".equals(group.getName())||"duplicates".equals(group.getName()))
                    continue;
                results.addAll(scan(group.getAbsolutePath())) ;
            }
        }
        return results;

    }


}
