package jit.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorksheetReferDetector {

    public static void main(String[] args) {

    }

    public static boolean isSelfWorksheetReference(List<String> worksheetNames, String formula) {
        if (worksheetNames == null || worksheetNames.size() == 0 || formula == null || formula.length() == 0 || formula.indexOf('!') == -1)
            return false;

        String patterns = "";
        for (String worksheetName :worksheetNames)
            patterns += "|(" + "^" + worksheetName + "!(\\$?[A-Z]+\\$?\\d+)$)";
        patterns = patterns.substring(1);

        Pattern r = Pattern.compile(patterns);
        Matcher m = r.matcher(formula);

        return m.find();
    }

    public static boolean isWorksheetReference(List<String> worksheetNames, String formula) {
        if (worksheetNames == null || worksheetNames.size() == 0 || formula == null || formula.length() == 0 || formula.indexOf('!') == -1)
            return false;
        return formula.indexOf("!")!=-1;
    }

    public static int getWReferCount(List<String> worksheetNames, String formula) {
        if (worksheetNames == null || worksheetNames.size() == 0 || formula == null || formula.length() == 0 || formula.indexOf('!') == -1)
            return 0;
        String patterns = "";
        for (String worksheetName :worksheetNames)
            patterns += "|(" + worksheetName + "!)";
        patterns = patterns.substring(1);

        Pattern r = Pattern.compile(patterns);
        Matcher m = r.matcher(formula);

        int count = 0;
        while (m.find()) {
            count++;
        }

        return count;
    }

}
