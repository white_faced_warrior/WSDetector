package jit.utils;

import com.google.gson.Gson;


import java.lang.reflect.Type;

public class JsonUtil {
    public static Gson gson = new Gson();

    public static void main(String[] args) {
    }

    public static String toJson(Object obj) {
        return gson.toJson(obj);
    }

    public static Object toJava(String json, Type type) {
        return gson.fromJson(json, type);
    }


}
