package jit.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileManager {

    public static void main(String[] args) {
        copyFolder("K:\\最终版","G:\\最终版");
    }

    public static void deleteFile(String path, String filename) {
        File file = new File(path + filename);
        if (file.getAbsoluteFile().exists())
            file.getAbsoluteFile().delete();
    }

    public static void deleteFloder(String path) {

        File f = new File(path);// 定义文件路径
        if (f.exists() && f.isDirectory()) {// 判断是文件还是目录
         if (f.listFiles().length == 0) {// 若目录下没有文件则直接删除
                f.delete();
            } else {// 若有则把文件放进数组，并判断是否有下级目录
                File delFile[] = f.listFiles();
                int i = f.listFiles().length;
                for (int j = 0; j < i; j++) {
                    if (delFile[j].isDirectory()) {
                        deleteFloder(delFile[j].getAbsolutePath());// 递归调用del方法并取得子目录路径
                    } else
                        delFile[j].delete();// 删除文件
                }
            }
        }

    }

    public static void createTxt(String path, String fileName, String content) {

        try {

            File bpath = new File(path);

            if (!bpath.exists())
                bpath.mkdirs();

            File writename = new File(path + fileName);
            if (!writename.exists())
                writename.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(writename));
            out.write(content);
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static void createObjectTxt(String path, String fileName, Object obj) {

        try {

            File bpath = new File(path);

            if (!bpath.exists())
                bpath.mkdirs();

            File writename = new File(path + fileName);
            if (!writename.exists())
                writename.createNewFile();
            FileOutputStream fos = new FileOutputStream(path + fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            oos.close();
            fos.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static void appendTxt(String path, String fileName, String content) {

        try {

            File bpath = new File(path);

            if (!bpath.exists())
                bpath.mkdirs();

            File writename = new File(path + fileName);
            if (!writename.exists())
                writename.createNewFile();
            BufferedWriter out = new BufferedWriter(new FileWriter(writename, true));
            out.write(content);
            out.flush();
            out.close();

        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static void appendObjectTxt(String path, String fileName, Object obj) {

        try {
            File bpath = new File(path);
            if (!bpath.exists())
                bpath.mkdirs();

            File writename = new File(path + fileName);
            if (!writename.exists())
                writename.createNewFile();
            FileOutputStream fos = new FileOutputStream(path + fileName, true);
            ObjectOutputStream oos = null;

            if (writename.length() < 1) {
                oos = new ObjectOutputStream(fos);
            } else {
                oos = new MyObjectOutputStream(fos);
            }
            oos.writeObject(obj);
            oos.close();
            fos.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static void copyFile(String oldFilePathAndName, String newFilePathAndName) {
        System.out.println(oldFilePathAndName);
        oldFilePathAndName = oldFilePathAndName.trim();
        oldFilePathAndName = oldFilePathAndName.replaceAll("\\\\", "/");
        newFilePathAndName = newFilePathAndName.replaceAll("\\\\", "/");
        String newpath = newFilePathAndName.substring(0, newFilePathAndName.lastIndexOf("/"));
        int byteread = 0;
        try {
            File oldFile = new File(oldFilePathAndName);
            if (!oldFile.exists())
                return;
            File newp = new File(newpath);
            if (!newp.exists())
                newp.mkdirs();
            File newFile = new File(newFilePathAndName);
            newFile.createNewFile();
            FileInputStream inStream = new FileInputStream(oldFilePathAndName);
            FileOutputStream outStream = new FileOutputStream(newFile);
            byte buffer[] = new byte[1024];
            while ((byteread = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, byteread);
            }
            inStream.close();
            outStream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static List<String> readText(String filePathAndName) {
        List<String> list = new ArrayList<String>();

        FileInputStream fs;
        try {
            File file = new File(filePathAndName);
            if (file.exists()) {
                fs = new FileInputStream(filePathAndName);
                InputStreamReader isr;
                isr = new InputStreamReader(fs);
                BufferedReader br = new BufferedReader(isr);
                String data = "";
                int i = 0;
                while ((data = br.readLine()) != null) {
                    list.add(i++, data);
                }
                fs.close();
            } else {
                System.out.println(filePathAndName + " is not exsit");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }

        return list;

    }

    public static List<Object> readObjectText(String filePathAndName) {
        List<Object> list = new ArrayList<Object>();

        FileInputStream fs;
        try {
            File file = new File(filePathAndName);
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(filePathAndName);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Object data = "";
                int i = 0;
                while ((data = ois.readObject()) != null) {
                    list.add(i++, data);
                }
                ois.close();
                fis.close();
            } else {
                System.out.println(filePathAndName + " is not exsit");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            // e.printStackTrace();

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
        }

        return list;

    }

    public static List<String> readText(String filePathAndName, String code) {
        List<String> list = new ArrayList<String>();

        FileInputStream fs;
        try {
            File file = new File(filePathAndName);
            if (file.exists()) {
                fs = new FileInputStream(filePathAndName);
                InputStreamReader isr;
                isr = new InputStreamReader(fs, code);
                BufferedReader br = new BufferedReader(isr);
                String data = "";
                int i = 0;
                while ((data = br.readLine()) != null) {
                    list.add(i, data);
                }
                fs.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }
        Collections.reverse(list);
        return list;

    }

    public static void copyFolder(String oldFolderPath, String newFolderPath) {
        oldFolderPath = oldFolderPath.trim();
        newFolderPath = newFolderPath.trim();
        File oldFolder = new File(oldFolderPath);
        File newFolder = new File(newFolderPath);
        if (!newFolder.exists()) {
            newFolder.mkdirs();
        }
        if (!oldFolder.exists()) {
            System.out.println("error info:" + oldFolderPath + " is not exist");
            return;
        }
        if (newFolder.isFile()) {
            System.out.println("error info:" + newFolderPath + " is not directory");
            return;
        }
        if (oldFolder.isFile()) {
            System.out.println("error info:" + oldFolderPath + " is not  not directory");
            return;
        }
        String fileList[] = oldFolder.list();
        for (int i = 0; i < fileList.length; i++) {
            File temp = null;

            temp = new File(oldFolderPath + fileList[i]);

            if (temp.isFile()) {
                copyFile(oldFolderPath + fileList[i], newFolderPath + fileList[i]);
            }
            if (temp.isDirectory()) {
                copyFolder(oldFolderPath + fileList[i] + "/", newFolderPath + fileList[i] + "/");
            }
        }
        System.out.println("Done");

    }
    public static void mkdirs(String path){
        File bpath = new File(path);

        if (!bpath.exists())
            bpath.mkdirs();
    }


}
