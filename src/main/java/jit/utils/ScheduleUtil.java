package jit.utils;

import jit.constants.Constants;

import java.util.List;

public class ScheduleUtil{
    public static void main(String[] args) {

    }

    public static  List<String> remove_had_done(String path,List<String> todos){
        List<String> dones=FileManager.readText(path+"schedule/"+Constants.schedul_file);
        todos.removeAll(dones);
        return todos;
    }

    public static void mark_done(String path,String item){
        if(!item.endsWith("\n"))
            item+="\n";
        FileManager.appendTxt(path+"schedule/",Constants.schedul_file,item);
    }

    public static void clean_all_dones(){
        FileManager.deleteFile(Constants.Enron_baseDir,Constants.schedul_file);
    }
}
