package jit.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jit.models.WSheet;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.format.CellFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class POIUtil {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static Workbook createSpreadsheet2003() {
        HSSFWorkbook workbook = new HSSFWorkbook();
        return workbook;
    }

    public static Workbook createSpreadsheet2007() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        return workbook;
    }

    public static Workbook openSpreadsheet(String pathAndFileName) {
        File file = new File(pathAndFileName);
        if (!file.exists()) {
            System.out.println("openSpreadsheet Info: Spreadsheet is not existed");
            return null;
        }
        FileInputStream in;
        Workbook book = null;
        try {
            in = new FileInputStream(pathAndFileName);
            book = WorkbookFactory.create(file);

        } catch (RuntimeException | IOException e) {
            //e.printStackTrace();

        } finally {
            return book;
        }

    }

    public static void saveSpreadsheet(Workbook workbook, String pathAndFileName) {
        pathAndFileName = pathAndFileName.trim();
        if (!pathAndFileName.endsWith(".xls") && !pathAndFileName.endsWith(".xlsx")) {
            System.out.println("saveSpreadsheet Info: File Name must end with '.xls' or '.xlsx'  ");
            return;
        }
        pathAndFileName = pathAndFileName.replaceAll("\\\\", "/");
        String dirPath = pathAndFileName.substring(0, pathAndFileName.lastIndexOf("/"));

        try {
            File dir = new File(dirPath);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(pathAndFileName);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream excel = new FileOutputStream(file);
            workbook.write(excel);
            workbook.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    public static String getStringValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        String strCell = "";

        switch (cell.getCellType()) {
            case STRING:
                strCell = cell.getStringCellValue();
                break;
            case NUMERIC:
                strCell = String.valueOf(cell.getNumericCellValue());
                break;
            case FORMULA:
                try {
                    strCell = String.valueOf(cell.getRichStringCellValue());
                } catch (IllegalStateException e) {
                    if (e.getMessage() != null && e.getMessage().indexOf("error formula cell") != -1)
                        strCell = String.valueOf(cell.getErrorCellValue());
                    else if (e.getMessage() != null && e.getMessage().indexOf("boolean formula cell") != -1) {
                        strCell = String.valueOf(cell.getBooleanCellValue());
                    } else if (e.getMessage() != null && e.getMessage().indexOf("from a text cell") != -1) {
                        strCell = String.valueOf(cell.getStringCellValue());
                    } else if (e.getMessage() != null && e.getMessage().indexOf("from a numeric formula cell") != -1) {
                        strCell = String.valueOf(cell.getNumericCellValue());
                    } else
                        strCell = "";
                }
                break;
            case BOOLEAN:
                // strCell = String.valueOf(cell.getBooleanCellValue());
                break;
            case ERROR:
                // strCell = String.valueOf(cell.getErrorCellValue());
                break;
            case BLANK:
                break;
            default:
                break;
        }
        if (strCell == null || strCell.trim().length() == 0) {
            return "";
        }

        return strCell;
    }

    public static List<Integer> getEmptyRows(Sheet sheet) {
        List<Integer> erows = new ArrayList<>();
        if (sheet == null)
            return null;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null) {
                erows.add(erows.size(), r);
                continue;
            }
            boolean isEmpty = true;
            for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
                Cell tcell = row.getCell(c);
                if (tcell == null)
                    continue;
                String value = getStringValue(tcell);
                if (value.length() > 0) {
                    isEmpty = false;
                    break;
                }
            }
            if (isEmpty)
                erows.add(erows.size(), r);
        }
        return erows;

    }

    public static List<Integer> getEmptyCols(Sheet sheet) {
        List<Integer> ecols = new ArrayList<>();
        int min_col = Integer.MIN_VALUE;
        int max_col = Integer.MIN_VALUE;
        if (sheet == null)
            return null;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null) {
                continue;
            }
            boolean isEmpty = true;
            if (row.getFirstCellNum() < min_col)
                min_col = row.getFirstCellNum();
            if (row.getLastCellNum() > max_col)
                max_col = row.getLastCellNum();
        }
        for (int c = min_col; c <= max_col; c++) {
            boolean isEmpty = true;
            for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {

                Row row = sheet.getRow(r);
                if (row == null) {
                    continue;
                }
                Cell tcell = row.getCell(c);
                if (tcell == null)
                    continue;
                String value = getStringValue(tcell);
                if (value.length() > 0) {
                    isEmpty = false;
                    break;
                }
            }
            if (isEmpty)
                ecols.add(ecols.size(), c);
        }
        return ecols;
    }
    public static String getShowValue(Cell cell){
        String showValue= CellFormat.getInstance(cell.getCellStyle().getDataFormatString()).apply(cell).text;
        return showValue;
    }
    public static int getAccuracy(String number){
        if(number.indexOf(".")==-1)
            return 0;
        if(number.endsWith("%")){
            number=number.substring(0,number.length()-1);
        }
        return number.length()-number.indexOf(".")-1;

    }
}
