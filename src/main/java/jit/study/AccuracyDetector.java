package jit.study;

import jit.models.WBook;
import jit.models.WCell;
import jit.models.WSheet;
import jit.utils.*;

import java.util.List;

public class AccuracyDetector extends Detector{
    public static void main(String[] args) {
        AccuracyDetector check=new AccuracyDetector("Enron");
//        System.out.println(check.get_output_path());


    }

    public AccuracyDetector(String curpus_name ) {
        this.curpus_name=curpus_name;
        this.smell_type="DiffAccuracy";
    }

    public boolean detect(WBook book){
        WBook tbook=null;
        for(WSheet sheet :book.getSheets()){
            WSheet tsheet=null;
            List<WCell> cells=sheet.getCells();
            for (WCell cell:cells){
                if(cell.isHasAccuracyProblem()){
                    if(tsheet==null)
                        tsheet= new WSheet(sheet.getName());
                    tsheet.addCell(cell);
                }
            }
            if(tsheet!=null){
                if(tbook==null){
                    tbook =new WBook(book.getPath());
                }
                tbook.addSheet(tsheet);
            }

        }
        if(tbook!=null){
            ExcelMarker.mark_fillter_out_target_excels(tbook,get_output_path(),index);
            SaveToTxt(tbook);
            index++;
            return true;
        }
        return false;
    }





}
