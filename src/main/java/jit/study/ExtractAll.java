package jit.study;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import jit.models.WBook;
import jit.models.WCell;
import jit.models.WSheet;
import jit.utils.POIUtil;
import jit.utils.WorksheetReferDetector;
import org.apache.poi.ss.usermodel.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExtractAll {
    private int simple_smell_count=0;
    private int total_smell_count=0;
    public static void main(String[] args) {

    }

    public HashMap<String,List<String>> extractBook(String file){
        HashMap<String,List<String>> results=new HashMap<String,List<String>>();
        Workbook book= POIUtil.openSpreadsheet(file);
        if(book == null ){
            return results ;
        }
        List<String> sheetNames=new ArrayList<>();
        int sheetNum=book.getNumberOfSheets();
        if(sheetNum==1){
            return results;
        }
        for (int i=0;i<sheetNum;i++){
            sheetNames.add(book.getSheetAt(i).getSheetName());
        }

        for(int i=0;i<sheetNum;i++){
            Sheet tSheet=book.getSheetAt(i);
            List<String> smells=extractSheet(tSheet,sheetNames);
            if(smells.size()>0)
                results.put(tSheet.getSheetName(),smells);
        }
        return results;
    }





    public List<String> extractSheet(Sheet sheet,  List<String> sheetNames){
        List<String> result=new ArrayList<>();
        if(sheet ==null)
            return result;
        for(int r=sheet.getFirstRowNum();r<=sheet.getLastRowNum();r++){
            Row row=sheet.getRow(r);
            if(row ==null)
                continue;
            for (int c=row.getFirstCellNum();c<=row.getLastCellNum();c++){
                Cell tcell=row.getCell(c);
                if(tcell==null)
                    continue;
                CellType cType= tcell.getCellType();
                if(!CellType.FORMULA.equals(cType))
                    continue;
                String formula=tcell.getCellFormula();

                boolean isSampleWRefer=WorksheetReferDetector.isSelfWorksheetReference(sheetNames,formula);
                if(isSampleWRefer){
                    result.add(tcell.getAddress().toString());
                    simple_smell_count++;
                    System.out.println(tcell.getAddress().toString());
                }else{
                    total_smell_count+=WorksheetReferDetector.getWReferCount(sheetNames,formula);
                }
            }
        }
        return result;
    }


}
