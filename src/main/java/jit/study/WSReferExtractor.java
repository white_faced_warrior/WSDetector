package jit.study;

import jit.models.WBook;
import jit.models.WCell;
import jit.models.WSheet;
import jit.utils.POIUtil;
import jit.utils.WorksheetReferDetector;
import org.apache.poi.ss.usermodel.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WSReferExtractor {
    public static void main(String[] args) {

    }

    public WBook extractBook(String file) {
        WBook rbook = new WBook(file);
        Workbook book = POIUtil.openSpreadsheet(file);
        if (book == null) {
            return null;
        }
        List<String> sheetNames = new ArrayList<>();
        int sheetNum = book.getNumberOfSheets();
        if (sheetNum == 1) {
            return null;
        }
        for (int i = 0; i < sheetNum; i++) {
            sheetNames.add(book.getSheetAt(i).getSheetName());
        }

        for (int i = 0; i < sheetNum; i++) {
            Sheet tSheet = book.getSheetAt(i);
            WSheet rsheet = extractSheet(tSheet, sheetNames);
            if (rsheet != null)
                rbook.addSheet(rsheet);
        }
        if (!rbook.getSheets().isEmpty())
            return null;
        else
            return rbook;
    }

    public WSheet extractSheet  (Sheet sheet, List<String> sheetNames) {
        WSheet rsheet = new WSheet(sheet.getSheetName());

        if (sheet == null)
            return null;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null)
                continue;
            for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
                Cell tcell = row.getCell(c);
                if (tcell == null)
                    continue;
                CellType cType = tcell.getCellType();
                if (!CellType.FORMULA.equals(cType))
                    continue;
                String formula = tcell.getCellFormula();
                boolean isSampleWRefer = WorksheetReferDetector.isSelfWorksheetReference(sheetNames, formula);
                if (isSampleWRefer) {
                    WCell simple_cell = new WCell(tcell);
                    //rsheet.addSimple_refers(simple_cell);
                } else {
                    int count = WorksheetReferDetector.getWReferCount(sheetNames, formula);
                    if (count >= 1) {
                        WCell complex_cell = new WCell(tcell);
                     //   rsheet.addComplex_refers(complex_cell, count);
                    }
                }
            }
        }
        if (!rsheet.getCells().isEmpty())
            return null;
        else
            return rsheet;

    }
}
