package jit.study;

import jit.constants.Constants;
import jit.models.WBook;
import jit.utils.FileManager;
import jit.utils.JsonUtil;

public abstract class Detector {
    public String smell_type="";
    public String curpus_name="";
    public  String txt_fileName="records.txt";
    public int index=1;
    public void SaveToTxt(WBook book){
        String json= JsonUtil.toJson(book);
        FileManager.appendTxt(get_output_path(),getTxt_fileName(),json+"\n");
    }

    public String get_output_path() {
        return Constants.marked_dir+smell_type+"/"+curpus_name+"/";
    }

    public String getTxt_fileName(){
        return smell_type+"_"+txt_fileName;
    }

    public abstract boolean detect(WBook book);

    public String getSmell_type() {
        return smell_type;
    }
}
