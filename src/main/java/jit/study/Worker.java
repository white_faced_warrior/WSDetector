package jit.study;

import com.google.gson.reflect.TypeToken;
import jit.constants.Constants;
import jit.models.WBook;
import jit.models.WCell;
import jit.models.WSheet;
import jit.utils.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Worker {
    private List<Detector> detectors=new ArrayList<>();
    public static void main(String[] args) {
        String path=Constants.Enron_baseDir;
        String curpus="Enron";
        AccuracyDetector accuracyDetector=new AccuracyDetector(curpus);
        DataErrorDetector dataErrorDetector=new DataErrorDetector(curpus);
        IFFormulaDetector ifFormulaDetector=new IFFormulaDetector(curpus);
        ReferFormulaDetector referFormulaDetector=new ReferFormulaDetector(curpus);
        Worker worker=new Worker();
        worker.addDetector(accuracyDetector);
        worker.addDetector(dataErrorDetector);
        worker.addDetector(ifFormulaDetector);
        worker.addDetector(referFormulaDetector);
        worker.run(path);
    }
    public void run(String path){
        HashMap<String,Integer> results=new HashMap<>();
        FloderScanner scanner =new FloderScanner(path);
        List<String> excels=scanner.startScan();
        int current=0;
        int smell=0;
        excels= ScheduleUtil.remove_had_done(path,excels);
        for(String excel :excels){
            ScheduleUtil.mark_done(path,excel);
            Worker worker=new Worker();
            WBook wBook=worker.extractBook(excel);
            if(wBook!=null){
                for (Detector detector:detectors){
                   boolean result= detector.detect(wBook);
                   if(result){
                       if(results.containsKey(detector.getSmell_type()))
                           results.put(detector.getSmell_type(),results.get(detector.getSmell_type())+1);
                       else
                           results.put(detector.getSmell_type(),1);
                   }
                }
            }

            current+=1;
            System.out.println("Total:"+excels.size()+"  Current:"+current);
        }
        for (String key :results.keySet()){
            System.out.println(key+":"+results.get(key));
        }
    }

    public WBook extractBook(String file) {
        WBook rbook = new WBook(file);
        Workbook book = POIUtil.openSpreadsheet(file);
        if (book == null) {
            return null;
        }
        List<String> sheetNames = new ArrayList<>();
        int sheetNum = book.getNumberOfSheets();
        if (sheetNum == 1) {
            return null;
        }
        for (int i = 0; i < sheetNum; i++) {
            sheetNames.add(book.getSheetAt(i).getSheetName());
        }

        for (int i = 0; i < sheetNum; i++) {
            Sheet tSheet = book.getSheetAt(i);
            WSheet rsheet = extractSheet(tSheet, sheetNames);
            if (rsheet!=null)
                rbook.addSheet(rsheet);
        }
        try {
            book.close();
        }catch (Exception ex){

        }

        if (rbook.getSheets().isEmpty())
            return null;
        else
            return rbook;
    }

    public WSheet extractSheet  (Sheet sheet, List<String> sheetNames) {
        WSheet rsheet = new WSheet(sheet.getSheetName());

        if (sheet == null)
            return null;
        for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
            Row row = sheet.getRow(r);
            if (row == null)
                continue;
            for (int c = row.getFirstCellNum(); c <= row.getLastCellNum(); c++) {
                if(c<0)
                    continue;
                Cell cell = row.getCell(c);
                if (cell == null)
                    continue;
                WCell tcell=new WCell(cell);
                if(tcell.isHasAccuracyProblem()||tcell.isHasDataError()||tcell.isIFFormula()||tcell.isReferFormula())
                    rsheet.addCell(tcell);
            }
        }
        if (rsheet.getCells().isEmpty())
            return null;
        else
            return rsheet;

    }

    public void addDetector(Detector detector){
        this.detectors.add(detector);
    }



}
